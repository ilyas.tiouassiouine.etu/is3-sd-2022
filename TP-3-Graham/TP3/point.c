/* point.h */

#if ! defined (POINT_H)
#define POINT_H 1

#include <stdbool.h>
#include "point.h"

/*
 * IMPLANTATION
 *
 * Un �l�ment de type struct point repr�sente un point dans le plan.
 * Les champs x et y donnent les coordonn�es cart�siennes.
 * Le champ ident contient une lettre, pour identifier le point.
 */

// struct point
// {
//   double x;                     /* abscisse */
//   double y;                     /* ordonn�e */
//   char ident;                   /* identificateur */
// };

/*
 * TYPE ABSTRAIT
 */

/*
 * Constructeur
 */

void init_point (struct point * p, double px, double py, char identif){
    (*p).x = px;
    (*p).y = py;    
    (*p).ident = identif;    
};

/*
 * Fonction de comparaison, pour trier un tableau de points par
 * angle croissant, au moyen de qsort.
 */

int compare_points (const void * p0, const void * q0){
    struct point * p = (struct point *)p0;
    struct point * q = (struct point *)q0;
    double det;
    det = (p->x)*(q->y) - (p->y)*(q->x);
    if (det > 0){return -1} else {if (det ==0){return 0;};};
    return 1;
};

/*
 * Retourne true si le chemin qui va de A vers C tourne � gauche en B.
 */

bool tourne_a_gauche (struct point *A,
                             struct point *B, struct point *C){
    double det;
    det = ((*B).x - (*A).x) * ((*C).y - (*A).y) - ((*B).y - (*A).y) * ((*C).x - (*A).x)
    return (det > 0);
                            };

#endif

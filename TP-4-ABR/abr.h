#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#if ! defined (NOEUD_H)
#define NOEUD_H 1

struct noeud {
    struct noeud * droite;
    struct noeud * gauche;    
    int valeur;
};

#define NIL (struct noeud *)0

extern struct noeud * init_abr (int);

extern struct noeud * ajouter_abr (struct noeud *, int);

extern void affichage_abr_croissant(struct noeud *);

extern void affichage_abr_indente(struct noeud *);

extern int max(int,int);

extern int hauteur_abr(struct noeud * );

extern int nb_noeud_abr(struct noeud *);

extern void clear_abr (struct noeud *);

extern bool rechercher_abr (int, struct noeud *);

extern bool rechercher_abr_pas_recursif (int, struct noeud *);

extern int somme_abr (struct noeud *);*

#endif

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#include "abr.h"


int main ()
{
    struct noeud *racine;
    int x;
    racine = init_abr (40);
    printf("Tape le x\n");
    scanf ("%d", &x);
    while (x!=-1){
        printf("Tape le x\n");
        racine = ajouter_abr (racine, x);
        affichage_abr_croissant (racine);
        printf("\n");
        printf("\n");
        affichage_abr_indente (racine);
        scanf ("%d", &x);
    }

    printf ("la hauteur de l'ABR est %d\n", hauteur_abr (racine));
    printf ("le nombre de noeuds de l'ABR est %d\n",
    nb_noeud_abr (racine));
    if (rechercher_abr(7,racine)){printf("trouvé");} else {printf("pas trouvé");};
    printf("\n La somme des valeurs est %d",somme_abr(racine));
    clear_abr (racine);
return 0;
}



#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include "abr.h"

struct noeud * init_abr (int value){
    struct noeud * premier;
    premier = (struct noeud *)malloc( sizeof(struct noeud));
    premier->gauche = NIL; 
    premier->droite = NIL; 
    premier->valeur = value;
    return premier;
}

struct noeud * ajouter_abr (struct noeud * ABR, int aajouter){
    if (ABR == NIL){
        return init_abr(aajouter);
    }
    else{
        assert (ABR->valeur != aajouter);
        if (ABR->valeur < aajouter){
            ABR->droite = ajouter_abr(ABR->droite, aajouter);
        }
        else {
            ABR->gauche = ajouter_abr(ABR->gauche, aajouter);        
        };
    };
    return ABR;
}

void affichage_abr_croissant(struct noeud * ABR){
    if (ABR != NIL){
        affichage_abr_croissant(ABR->gauche);
        printf("%d   ",ABR->valeur);
        affichage_abr_croissant(ABR->droite);        
    }
};

static void affichage_abr_indente_INNER(struct noeud * ABR, int n){
    if (ABR != NIL){
        affichage_abr_indente_INNER(ABR->droite, n+1);
        int i;
        for (i=0;i<n;i++){printf("    ");};
        printf("%d\n", ABR->valeur);
        affichage_abr_indente_INNER(ABR->gauche, n+1);
    }
}

void affichage_abr_indente(struct noeud * abr){
    affichage_abr_indente_INNER(abr, 0);
}

int max(int A, int B){
    if (A>B){return A;} else {return B;};
};

int hauteur_abr(struct noeud * ABR){
    if (ABR != NIL){
        return (max(hauteur_abr(ABR->droite), hauteur_abr(ABR->gauche)) + 1); 
    } else {return (-1);}
    
};

int nb_noeud_abr(struct noeud * ABR){
    if (ABR != NIL){
        return (nb_noeud_abr(ABR-> gauche) + nb_noeud_abr(ABR-> droite) + 1) ;
    }
    else {
        return 0;
    };
};

void clear_abr (struct noeud * ABR){
    if (ABR != NIL){
        clear_abr(ABR-> gauche);
        clear_abr(ABR-> droite);
        free(ABR);
    };
};


bool rechercher_abr_pas_recursif (int cle, struct noeud* A)
{
    bool trouve = false;
    struct noeud* N;

    N = A;
    while (N != NIL && !trouve)
      {
        if (N->valeur == cle)
            trouve = true;
        else if (N->valeur < cle)
            N = N->droite;
        else
            N = N->gauche;
      }
    return trouve;
};

bool rechercher_abr (int cle, struct noeud* ABR)
{
    bool trouve = false;
    if (ABR != NIL){
        if (ABR->valeur == cle){
            trouve = true;
        }
        else {
            if (cle > (ABR->valeur)){
                trouve = rechercher_abr(cle, ABR->droite);
            } 
            else {
                trouve = rechercher_abr(cle, ABR->gauche);
            };
        };
    };
    return trouve;
};

int somme_abr(struct noeud * ABR){
    int somme;
    if (ABR !=NIL){
        somme = ABR-> valeur + somme_abr(ABR->droite) + somme_abr(ABR->gauche);
        return somme;
    } else {return 0;};
};

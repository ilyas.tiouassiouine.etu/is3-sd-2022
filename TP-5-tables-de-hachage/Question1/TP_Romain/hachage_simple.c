#include <stdbool.h>
#include "hachage_simple.h"
#include "liste_double.h"


void init_table (struct table *T, fonction_hachage* h){
    T->hash=h;
    for (int i=0;i<N;i++)
        init_liste_double(&T->tab[i].L); /*l'adresse de champ L de la structure T->tab[i]*/
    
}


void clear_table (struct table *T){
    for (int i=0;i<N;i++)
        clear_liste_double(&T->tab[i].L);
}


void enregistrer_table (struct table *T, double x){
    int i=T->hash(x);
    ajouter_en_tete_liste_double ((&T->tab[i].L),x);
}


bool rechercher_table (struct table *T, double x){
    int i=T->hash(x);
    return (recherche_liste_double((&T->tab[i].L),x));       
}


void imprimer_table (struct table *T){
    for (int i=0;i<N;i++)
        imprimer_liste_double(&T->tab[i].L);
}

#include "hachage_simple.h"
#include <stdio.h>

void init_table (struct table * T, fonction_hachage * hach){
    T -> hash = hach;
    int i;
    for (i=0;i<N;i++){
        init_liste_double (&T->tab[i].L);
    };
};

void clear_table (struct table * T){
    int i;
    for (i=0;i<N;i++){
        clear_liste_double(&T->tab[i].L);
    };
};

void enregistrer_table (struct table * T, double d){
    int z;
    z = T->hash(d);
    ajouter_en_tete_liste_double (&(T->tab[z].L), d);
};

bool rechercher_table (struct table * T, double d){
    int z;
    z = T->hash(d);
    return recherche_liste_double (&(T->tab[z].L), d);
};

void imprimer_table (struct table * T){
    int i;
    for (i=0;i<N;i++){
        imprimer_liste_double (&(T->tab[i].L));
    }
};

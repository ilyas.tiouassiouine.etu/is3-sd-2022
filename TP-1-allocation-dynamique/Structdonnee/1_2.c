//indent -nut -i2.

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(){
    int c;
    char * s;
    int i;
    i = 0;      //nombre de caractères mis dans s
    int compt;  //compte le nombre de caractères encore tapables dans s
    s = (char *)malloc(8 * sizeof(char));
    compt = 8;
    c = getchar();
    while (!isspace(c)) 
        {
            if (i < compt-1){
                putchar(c);
                s[i] = (char)c;
                i = i + 1;
                c = getchar();
            }
            else {
                s = (char*)realloc(s, (compt + 8)* sizeof(char));
                compt = compt + 8;
            }
        }
    s[i] = '\0';
    putchar ('\n');
    printf("%s\n",s);
    free(s);
    return 0;
}

//indent -nut -i2.

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(){
    int c;
    char * s;
    int i;
    i = 0;
    s = (char *)malloc(64 * sizeof(char));
    c = getchar();
    while ((!isspace(c)) && (i < 63)) 
        {
            putchar(c);
            s[i] = (char)c;
            i = i + 1;
            c = getchar();
        }
    s[i] = '\0';
    putchar ('\n');
    printf("%s\n",s);
    free(s);
    return 0;
}

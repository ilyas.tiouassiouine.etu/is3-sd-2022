//#include <liste.h> 

struct chaine {
char * champ;       // chaque champ
int compte;         // nombre d'éléments dans le champ de chaine tapables
int i;
};

/*                                                                              ^
 * Constructeur.                     
 */

extern void init_chaine8 (struct chaine * s);


/*                                                                              
 * Ajout d'un caractère dans la chaine                                           
 */

extern void ajout_carac_chaine (struct chaine * s, char k);

extern void fin_ajout_carac_chaine (struct chaine * s);

//Rend plus longue la chaine 

// extern void alonge_chaine8 (struct chaine * s);


/*                                                                              
 * Impression.                                                                  
 */

extern void affiche_chaine (struct chaine * s);

/*                                                                              
 * Destructeur                                                                  
 */

extern void clear_chaine (struct chaine * s);

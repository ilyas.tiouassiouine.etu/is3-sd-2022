//indent -nut -i2.

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"

int main(){
    int c;
    struct chaine s;  
    init_chaine8(&s);
    c = getchar();
    while (!isspace(c))
        {
            ajout_carac_chaine(&s, c);
            c = getchar();
        }
    fin_ajout_carac_chaine(&s);
    putchar ('\n');
    affiche_chaine(&s);
    clear_chaine(&s);
    return 0;
}

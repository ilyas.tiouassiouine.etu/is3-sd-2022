
#include <stdio.h>
#include <stdlib.h>
#include "chaine.h"

/*                                                                              
 * Constructeur.                     
 */

void init_chaine8 (struct chaine * s){
    (*s).champ = (char *)malloc(8 * sizeof(char));
    (*s).compte = 8;
    (*s).i = 0;
};


/*                                                                              
 * Ajout d'un caractère dans la chaine                                           
 */

void ajout_carac_chaine (struct chaine * s, char c){  
    if ((*s).i < (*s).compte-1){
        (*s).champ[(*s).i] = (char)c;
        (*s).i = (*s).i + 1;
    }
    else {
        (*s).champ = (char*)realloc(s->champ, (s->compte + 8)* sizeof(char));
        (*s).compte = (*s).compte + 8;
        (*s).champ[(*s).i] = (char)c;
        (*s).i = (*s).i + 1;
    };
};

void fin_ajout_carac_chaine (struct chaine * s){
    (*s).champ[(*s).i] = '\0';
};


//Rend plus longue la chaine 

// extern void alonge_chaine8 (struct chaine * s){
//     (*s).champ = (char*)realloc((s*).champ, ((s*).compt + 8)* sizeof(char));
//     (*s).compte = (*s).compte + 8;
// };


/*                                                                              
 * Impression.                                                                  
 */

void affiche_chaine (struct chaine * s){
     printf("%s\n",(*s).champ);
};

/*                                                                              
 * Destructeur                                                                  
 */

void clear_chaine (struct chaine * s){
    free((*s).champ);
    (*s).compte = 0;
    (*s).i = 0;
};

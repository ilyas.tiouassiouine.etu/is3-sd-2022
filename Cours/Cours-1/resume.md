Résumé du CM 1 de SD
--------------------

Concevoir une SD = concevoir un nouveau type

Pour créer un nouveau type (rationnel) on écrit 
deux fichiers : rationnel.h et rationnel.c

L'ensemble { rationnel.h et rationnel.c } = le module "rationnel"
Suffixe "h" = "header" (entête en Français)

Dans un ".h" on trouve :
1. une déclaration de type (struct rationnel)
2. les prototypes des fonctions exportées par le module
3. les spécifications de la structure C et des fonctions exportées

spécifications = ce qui ne se lit pas directement dans le structure C
    (par exemple : denom > 0)

Les modes de passage de paramètres (D, R et D/R) c'est très important.
R ou D/R => passage par adresse en C
D => comme on veut (c'est plus général de passer les structures tjs par adresse)

On compile les ".c". On ne compile jamais les ".h"
On inclut les ".h" (#include). On n'inclut jamais un ".c"

On peut compiler séparément les différents fichiers ".c" d'une application :
gcc -c main.c
gcc -c rationnel.c
Le compilateur vérifie la syntaxe. Il fabrique des fichiers ".o" (objets)
qui sont des "exécutables à trous" (les trous = des références indéfinies).

On peut ensuite faire l'édition des liens des fichiers objets.
On obtient un exécutable (nommé par défaut "a.out")
gcc main.o rationnel.o

Quand on affecte une valeur v à une structure C, on distingue deux cas :
1. la structure a déjà une valeur, qu'on change avec v
2. la structure a un contenu aléatoire. On s'apprête à lui donner
    v pour première valeur.

Un constructeur est une fonction du type 2.

Jusqu'à présent, on n'a pas eu besoin de distinguer les cas mais
ça va devenir important dans les cours qui suivent.

Un destructeur est une action qui fait une dernière passe sur une
structure C qui est sur le point de disparaître et qui libère les
éventuelles ressources mobilisées par la structure.


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "liste.h"

void init_liste (struct liste* L)
  {
    L->tete = NIL;
    L->nbelem = 0;
  }

void clear_liste (struct liste* L)
  {
  }

void ajout_en_tete (struct liste* L, double d)
  {
    struct maillon* M;
// 1
    M = (struct maillon*)malloc (sizeof(struct maillon));
// 2
    M->valeur = d;
// 3
    M->suivant = L->tete;
// 4
    L->tete = M;
// 5
    L->nbelem += 1;
  }

void impression (struct liste L)
  {
    struct maillon* M;
    bool premier_maillon; // pour soigner l'affichage. Usage cosmétique
    printf ("[");
// 1
    M = L.tete;
// on parcourt tous les maillons : on tourne nbelem fois
    premier_maillon = true;
    for (int i = 0; i < L.nbelem; i++)
      {
        if (premier_maillon)
          printf ("%lf", M->valeur);
        else
          printf (", %lf", M->valeur);
        premier_maillon = false;
        M = M->suivant;
      }
    printf ("]\n");
  }







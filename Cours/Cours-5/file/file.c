// file.c

#include <assert.h>
#include "file.h"

void init_file (struct file* F)
{
    F->re = 0;
    F->we = 1;
}

void clear_file (struct file* F)
{
}

static bool est_pleine_file (struct file* F)
{
    return F->we == (F->re + 2) % TMAX;
}

void enfiler (struct file* F, double d)
{
    assert (! est_pleine_file (F));

    F->we = (F->we + TMAX - 1) % TMAX;
    F->tab [F->we] = d;
}

double defiler (struct file* F)
{
    double d;

    assert (! est_vide_file (F));

    d = F->tab [F->re];
    F->re = (F->re + TMAX - 1) % TMAX;
    return d;
}

bool est_vide_file (struct file* F)
{
    return F->we == (F->re + 1) % TMAX;
}


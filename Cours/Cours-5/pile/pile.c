// pile.c

#include <assert.h>
#include "pile.h"

void init_pile (struct pile* P)
{
    P->sp = -1;
}

void clear_pile (struct pile* P)
{
}

void empiler (struct pile* P, double d)
{
    assert (P->sp < TAILLE_MAX-1);
    P->sp += 1;
    P->tab [P->sp] = d;
/*
 * ou encore
    P->tab [++P->sp] = d;
 */
}

double depiler (struct pile* P)
{
    double d;

    assert (! est_vide_pile (P));

    d = P->tab [P->sp];
    P->sp -= 1;
    return d;
/*
 * ou encore
    return P->tab [P->sp--];
 */
}

bool est_vide_pile (struct pile* P)
{
    return P->sp == -1;
}


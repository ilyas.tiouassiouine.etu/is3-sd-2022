// abr.c

#include <stdlib.h>
#include <assert.h>
#include "abr.h"

void clear_ABR (struct noeud* A)
{
}

bool rechercher_ABR (struct noeud* A, int cle)
{
    bool trouve = false;
    struct noeud* N;

    N = A;
    while (N != NIL && !trouve)
      {
        if (N->valeur == cle)
            trouve = true;
        else if (N->valeur < cle)
            N = N->droite;
        else
            N = N->gauche;
      }
    return trouve;
}

/* fonction auxiliaire de ajouter_ABR qui crée une feuille */

static struct noeud* new_feuille (int cle)
{
    struct noeud* F;
    F = (struct noeud*)malloc (sizeof (struct noeud));
    F->valeur = cle;
    F->gauche = NIL;
    F->droite = NIL;
    return F;
}

struct noeud* ajouter_ABR (struct noeud* A, int cle)
{
    if (A == NIL)
        return new_feuille (cle);
    else /* on sait que A n'est pas vide */
      {
        struct noeud *prec, *cour;
        prec = NIL; /* indéfini */
        cour = A;
        while (cour != NIL)
          {
            assert (cour->valeur != cle); /* la clé est unique ! */
            prec = cour; /* on mémorise l'ancienne valeur de cour */
            if (cour->valeur < cle)
                cour = cour->droite;
            else
                cour = cour->gauche;
          }
        /* cour vaut NIL et prec = le noeud à modifier */
        if (prec->valeur < cle)
            prec->droite = new_feuille (cle);
        else
            prec->gauche = new_feuille (cle);
        /* on retourne l'ABR complet modifié */
        return A;
      }
}




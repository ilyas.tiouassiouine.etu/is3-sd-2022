// abr.h

struct noeud 
  {
    struct noeud* gauche;
    int valeur;
    struct noeud* droite;
  };

#define NIL (struct noeud*)0

extern void clear_ABR (struct noeud*);

#include <stdbool.h>

extern bool rechercher_ABR (struct noeud*, int);

extern struct noeud* ajouter_ABR (struct noeud*, int);
/*     <----------->
        type de retour
 */


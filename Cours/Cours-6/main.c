#include <stdio.h>
#include "abr.h"

int main ()
{
    int cle [] = { 89, 17, 41, 108, 53, 4 };
    int n = sizeof(cle)/sizeof(int);

    struct noeud* A;

    A = NIL;
    for (int i = 0; i < n; i++)
      {
        A = ajouter_ABR (A, cle[i]);
      }
    if (rechercher_ABR (A, 17))
      printf ("17 est dans l'ABR\n");
    else
      printf ("17 n'est pas dans l'ABR\n");
    clear_ABR (A);
    return 0;
}

